package org.green.kav.csv;

import org.green.kav.csv.model.CsvTypeModel;
import org.green.kav.csv.model.cdr.CdrCsvModel;
import org.green.kav.csv.service.CsvService;
import org.green.kav.csv.service.CsvTypeHandler;
import org.green.kav.csv.service.cdr.CdrCsvHandler;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class CsvReaderApplicationTests {

    @Autowired
    private CsvService csvService;

    @Test
    void contextLoads() throws Exception {
        String headers = "PROVIDER;EVENTTYPE;START;DURATION;STATUS;CGISTARTA;CGISTARTB;LATSTARTA;LATSTARTB;LONSTARTA;LONSTARTB;CGIENDA;CGIENDB;LATENDA;LATENDB;LONENDA;LONENDB;MSISDNA;MSISDNB;MSISDNC;IMEIA;IMEIB;IMSIA;IMSIB";
        String delimiter = ";";

        CsvTypeHandler csvTypeHandler = new CdrCsvHandler(headers, delimiter);
        List<CsvTypeModel> records = csvService.readCsv("P:\\work\\6. trovicor - 05 08 2019\\convoy-data\\c6-100 areas\\20230528165330180_mfi_cdr-100-area.csv",
                                                        csvTypeHandler);


        for (CsvTypeModel csvTypeModel : records) {
            forceOverrideValues((CdrCsvModel) csvTypeModel);
        }

        csvService.writeToCsvFile("P:\\work\\6. trovicor - 05 08 2019\\convoy-data\\c6-100 areas",
                                  "updated-20230528165330180_mfi_cdr-100-area.csv",
                                  csvTypeHandler,
                                  records);
    }


    private void forceOverrideValues(CdrCsvModel csvTypeModel) {
        csvTypeModel.setCgiStartB("219-01-4155-2203328");
        csvTypeModel.setCgiEndB("219-01-4155-2203328");
        csvTypeModel.setLatStartB("43.506872");
        csvTypeModel.setLatEndB("43.506872");
        csvTypeModel.setLonStartB("16.40078");
        csvTypeModel.setLonEndB("16.40078");
    }

}
