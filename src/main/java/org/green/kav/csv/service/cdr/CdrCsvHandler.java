package org.green.kav.csv.service.cdr;

import org.green.kav.csv.model.CsvTypeModel;
import org.green.kav.csv.model.cdr.CdrCsvModel;
import org.green.kav.csv.service.CsvTypeHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CdrCsvHandler implements CsvTypeHandler {

    private final Map<Integer, String> headerIndex = new HashMap<>();
    private final String headers;
    private final String delimiter;

    public CdrCsvHandler(String headers, String delimiters) {
        this.headers = headers;
        this.delimiter = delimiters;
    }

    @Override
    public String getDelimiter() {
        return delimiter;
    }

    public String getHeaders() {
        return headers;
    }

    public List<String> getDefaultHeaders() {
        return Arrays.asList(headers.split(getDelimiter()));
    }

    @Override
    public void assignColumnIndexMapper(String[] headers) throws Exception {
        for (int i = 0; i < headers.length; i++) {
            String column = headers[i].toUpperCase();
            String headerName = getDefaultHeaders().contains(column) == true ? column : null;
            if (headerName == null) throw new Exception("colum [" + column + "] not supported");
            headerIndex.put(i, headerName);
        }
    }

    @Override
    public CsvTypeModel extractValue(String[] values) {
        CdrCsvModel cdrCsvModel = new CdrCsvModel();
        for (int i = 0; i < values.length; i++) {
            assignValue(headerIndex.get(i), values[i], cdrCsvModel);
        }
        return cdrCsvModel;
    }

    @Override
    public String parse(CsvTypeModel csvModel) {
        CdrCsvModel cdrCsvModel = (CdrCsvModel) csvModel;
        List<String> headerSequence = new ArrayList<>(headerIndex.values());
        StringBuilder sb = new StringBuilder();

        for (String header : headerSequence) {
            switch (header) {
                case "PROVIDER":
                    sb.append(cdrCsvModel.getProvider()).append(delimiter);
                    break;
                case "EVENTTYPE":
                    sb.append(cdrCsvModel.getEventType()).append(delimiter);
                    break;
                case "START":
                    sb.append(cdrCsvModel.getStart()).append(delimiter);
                    break;
                case "DURATION":
                    sb.append(cdrCsvModel.getDuration()).append(delimiter);
                    break;
                case "STATUS":
                    sb.append(cdrCsvModel.getStatus()).append(delimiter);
                    break;
                case "CGISTARTA":
                    sb.append(cdrCsvModel.getCgiStartA()).append(delimiter);
                    break;
                case "CGISTARTB":
                    sb.append(cdrCsvModel.getCgiStartB()).append(delimiter);
                    break;
                case "LATSTARTA":
                    sb.append(cdrCsvModel.getLatStartA()).append(delimiter);
                    break;
                case "LATSTARTB":
                    sb.append(cdrCsvModel.getLatStartB()).append(delimiter);
                    break;
                case "LONSTARTA":
                    sb.append(cdrCsvModel.getLonStartA()).append(delimiter);
                    break;
                case "LONSTARTB":
                    sb.append(cdrCsvModel.getLonStartB()).append(delimiter);
                    break;
                case "CGIENDA":
                    sb.append(cdrCsvModel.getCgiEndA()).append(delimiter);
                    break;
                case "CGIENDB":
                    sb.append(cdrCsvModel.getCgiEndB()).append(delimiter);
                    break;
                case "LATENDA":
                    sb.append(cdrCsvModel.getLatEndA()).append(delimiter);
                    break;
                case "LATENDB":
                    sb.append(cdrCsvModel.getLatEndB()).append(delimiter);
                    break;
                case "LONENDA":
                    sb.append(cdrCsvModel.getLonEndA()).append(delimiter);
                    break;
                case "LONENDB":
                    sb.append(cdrCsvModel.getLonEndB()).append(delimiter);
                    break;
                case "MSISDNA":
                    sb.append(cdrCsvModel.getMsisdnA()).append(delimiter);
                    break;
                case "MSISDNB":
                    sb.append(cdrCsvModel.getMsisdnB()).append(delimiter);
                    break;
                case "MSISDNC":
                    sb.append(cdrCsvModel.getMsisdnC()).append(delimiter);
                    break;
                case "IMEIA":
                    sb.append(cdrCsvModel.getImeiA()).append(delimiter);
                    break;
                case "IMEIB":
                    sb.append(cdrCsvModel.getImeiB()).append(delimiter);
                    break;
                case "IMSIA":
                    sb.append(cdrCsvModel.getImsiA()).append(delimiter);
                    break;
                case "IMSIB":
                    sb.append(cdrCsvModel.getImsiB()).append(delimiter);
                    break;
                default:
                    // Handle default case (if necessary)
                    break;
            }
        }
        return sb.substring(0, sb.length() - 1); // Remove the last delimiter
    }

    public void assignValue(String headerName, String value, CdrCsvModel cdrCsvModel) {
        switch (headerName) {
            case "PROVIDER":
                cdrCsvModel.setProvider(value);
                break;
            case "EVENTTYPE":
                cdrCsvModel.setEventType(value);
                break;
            case "START":
                cdrCsvModel.setStart(value);
                break;
            case "DURATION":
                cdrCsvModel.setDuration(value);
                break;
            case "STATUS":
                cdrCsvModel.setStatus(value);
                break;
            case "CGISTARTA":
                cdrCsvModel.setCgiStartA(value);
                break;
            case "CGISTARTB":
                cdrCsvModel.setCgiStartB(value);
                break;
            case "LATSTARTA":
                cdrCsvModel.setLatStartA(value);
                break;
            case "LATSTARTB":
                cdrCsvModel.setLatStartB(value);
                break;
            case "LONSTARTA":
                cdrCsvModel.setLonStartA(value);
                break;
            case "LONSTARTB":
                cdrCsvModel.setLonStartB(value);
                break;
            case "CGIENDA":
                cdrCsvModel.setCgiEndA(value);
                break;
            case "CGIENDB":
                cdrCsvModel.setCgiEndB(value);
                break;
            case "LATENDA":
                cdrCsvModel.setLatEndA(value);
                break;
            case "LATENDB":
                cdrCsvModel.setLatEndB(value);
                break;
            case "LONENDA":
                cdrCsvModel.setLonEndA(value);
                break;
            case "LONENDB":
                cdrCsvModel.setLonEndB(value);
                break;
            case "MSISDNA":
                cdrCsvModel.setMsisdnA(value);
                break;
            case "MSISDNB":
                cdrCsvModel.setMsisdnB(value);
                break;
            case "MSISDNC":
                cdrCsvModel.setMsisdnC(value);
                break;
            case "IMEIA":
                cdrCsvModel.setImeiA(value);
                break;
            case "IMEIB":
                cdrCsvModel.setImeiB(value);
                break;
            case "IMSIA":
                cdrCsvModel.setImsiA(value);
                break;
            case "IMSIB":
                cdrCsvModel.setImsiB(value);
                break;
            default:
                // Handle default case (if necessary)
                break;
        }
    }
}
