package org.green.kav.csv.service;

import org.green.kav.csv.model.CsvTypeModel;
import org.green.kav.csv.model.cdr.CdrCsvModel;

public interface CsvTypeHandler {

    String getDelimiter();

    String getHeaders();
    void assignColumnIndexMapper(String[] headers) throws Exception;

    CsvTypeModel extractValue(String[] rowValues);

    String parse(CsvTypeModel csvModel);
}
