package org.green.kav.csv.service;

import org.green.kav.csv.model.CsvTypeModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CsvService {
    private Logger logger = LoggerFactory.getLogger(getClass());

    public List<CsvTypeModel> readCsv(String inputFile, CsvTypeHandler csvTypeHandler) throws Exception {
        logger.info("Processing {} ...", inputFile);
        long startTime = System.currentTimeMillis();
        List<CsvTypeModel> collectedRecords = new ArrayList<>();
        try (CSVReader reader = new CSVReaderBuilder(new FileReader(inputFile))
                .withCSVParser(new CSVParserBuilder().withSeparator(csvTypeHandler.getDelimiter().charAt(0)).build()).build()) {
            // Get only the header here
            csvTypeHandler.assignColumnIndexMapper(reader.readNext());
            String[] rowValues;
            for (int count = 0; (rowValues =  reader.readNext()) != null; count++) {
                CsvTypeModel csvTypeModel = csvTypeHandler.extractValue(rowValues);
                collectedRecords.add(csvTypeModel);
            }

            logger.info("Records loaded into database in {} ms", System.currentTimeMillis() - startTime);
        }
        logger.info("Completed in {} ms.", System.currentTimeMillis() - startTime);
        return collectedRecords;
    }

    public void writeToCsvFile(String path, String fileName, CsvTypeHandler csvTypeHandler, List<CsvTypeModel> models) {
        String newFilePath = path + System.getProperty("file.separator") + fileName;
        StringBuilder sb = new StringBuilder();
        sb.append(csvTypeHandler.getHeaders()).append("\n");

        for (CsvTypeModel csvModel: models) {
            sb.append(csvTypeHandler.parse(csvModel)).append("\n");
        }

        try (FileWriter fileWriter = new FileWriter(newFilePath)) {
            fileWriter.write(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.info("File Written in: {}", newFilePath);
    }
}
