package org.green.kav.csv.model.cdr;

import org.green.kav.csv.model.CsvTypeModel;

public class CdrCsvModel implements CsvTypeModel {
    private String provider;
    private String eventType;
    private String start;
    private String duration;
    private String status;
    private String cgiStartA;
    private String cgiStartB;
    private String latStartA;
    private String latStartB;
    private String lonStartA;
    private String lonStartB;
    private String cgiEndA;
    private String cgiEndB;
    private String latEndA;
    private String latEndB;
    private String lonEndA;
    private String lonEndB;
    private String msisdnA;
    private String msisdnB;
    private String msisdnC;
    private String imeiA;
    private String imeiB;
    private String imsiA;
    private String imsiB;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCgiStartA() {
        return cgiStartA;
    }

    public void setCgiStartA(String cgiStartA) {
        this.cgiStartA = cgiStartA;
    }

    public String getCgiStartB() {
        return cgiStartB;
    }

    public void setCgiStartB(String cgiStartB) {
        this.cgiStartB = cgiStartB;
    }

    public String getLatStartA() {
        return latStartA;
    }

    public void setLatStartA(String latStartA) {
        this.latStartA = latStartA;
    }

    public String getLatStartB() {
        return latStartB;
    }

    public void setLatStartB(String latStartB) {
        this.latStartB = latStartB;
    }

    public String getLonStartA() {
        return lonStartA;
    }

    public void setLonStartA(String lonStartA) {
        this.lonStartA = lonStartA;
    }

    public String getLonStartB() {
        return lonStartB;
    }

    public void setLonStartB(String lonStartB) {
        this.lonStartB = lonStartB;
    }

    public String getCgiEndA() {
        return cgiEndA;
    }

    public void setCgiEndA(String cgiEndA) {
        this.cgiEndA = cgiEndA;
    }

    public String getCgiEndB() {
        return cgiEndB;
    }

    public void setCgiEndB(String cgiEndB) {
        this.cgiEndB = cgiEndB;
    }

    public String getLatEndA() {
        return latEndA;
    }

    public void setLatEndA(String latEndA) {
        this.latEndA = latEndA;
    }

    public String getLatEndB() {
        return latEndB;
    }

    public void setLatEndB(String latEndB) {
        this.latEndB = latEndB;
    }

    public String getLonEndA() {
        return lonEndA;
    }

    public void setLonEndA(String lonEndA) {
        this.lonEndA = lonEndA;
    }

    public String getLonEndB() {
        return lonEndB;
    }

    public void setLonEndB(String lonEndB) {
        this.lonEndB = lonEndB;
    }

    public String getMsisdnA() {
        return msisdnA;
    }

    public void setMsisdnA(String msisdnA) {
        this.msisdnA = msisdnA;
    }

    public String getMsisdnB() {
        return msisdnB;
    }

    public void setMsisdnB(String msisdnB) {
        this.msisdnB = msisdnB;
    }

    public String getMsisdnC() {
        return msisdnC;
    }

    public void setMsisdnC(String msisdnC) {
        this.msisdnC = msisdnC;
    }

    public String getImeiA() {
        return imeiA;
    }

    public void setImeiA(String imeiA) {
        this.imeiA = imeiA;
    }

    public String getImeiB() {
        return imeiB;
    }

    public void setImeiB(String imeiB) {
        this.imeiB = imeiB;
    }

    public String getImsiA() {
        return imsiA;
    }

    public void setImsiA(String imsiA) {
        this.imsiA = imsiA;
    }

    public String getImsiB() {
        return imsiB;
    }

    public void setImsiB(String imsiB) {
        this.imsiB = imsiB;
    }

    @Override
    public String toString() {
        return "CsvModel{" +
                "provider='" + provider + '\'' +
                ", eventType='" + eventType + '\'' +
                ", start='" + start + '\'' +
                ", duration='" + duration + '\'' +
                ", status='" + status + '\'' +
                ", cgiStartA='" + cgiStartA + '\'' +
                ", cgiStartB='" + cgiStartB + '\'' +
                ", latStartA='" + latStartA + '\'' +
                ", latStartB='" + latStartB + '\'' +
                ", lonStartA='" + lonStartA + '\'' +
                ", lonStartB='" + lonStartB + '\'' +
                ", cgiEndA='" + cgiEndA + '\'' +
                ", cgiEndB='" + cgiEndB + '\'' +
                ", latEndA='" + latEndA + '\'' +
                ", latEndB='" + latEndB + '\'' +
                ", lonEndA='" + lonEndA + '\'' +
                ", lonEndB='" + lonEndB + '\'' +
                ", msisdnA='" + msisdnA + '\'' +
                ", msisdnB='" + msisdnB + '\'' +
                ", msisdnC='" + msisdnC + '\'' +
                ", imeiA='" + imeiA + '\'' +
                ", imeiB='" + imeiB + '\'' +
                ", imsiA='" + imsiA + '\'' +
                ", imsiB='" + imsiB + '\'' +
                '}';
    }

    public CdrCsvModel deepClone() {
        CdrCsvModel clonedObject = new CdrCsvModel();
        clonedObject.setProvider(this.provider);
        clonedObject.setEventType(this.eventType);
        clonedObject.setStart(this.start);
        clonedObject.setDuration(this.duration);
        clonedObject.setStatus(this.status);
        clonedObject.setCgiStartA(this.cgiStartA);
        clonedObject.setCgiStartB(this.cgiStartB);
        clonedObject.setLatStartA(this.latStartA);
        clonedObject.setLatStartB(this.latStartB);
        clonedObject.setLonStartA(this.lonStartA);
        clonedObject.setLonStartB(this.lonStartB);
        clonedObject.setCgiEndA(this.cgiEndA);
        clonedObject.setCgiEndB(this.cgiEndB);
        clonedObject.setLatEndA(this.latEndA);
        clonedObject.setLatEndB(this.latEndB);
        clonedObject.setLonEndA(this.lonEndA);
        clonedObject.setLonEndB(this.lonEndB);
        clonedObject.setMsisdnA(this.msisdnA);
        clonedObject.setMsisdnB(this.msisdnB);
        clonedObject.setMsisdnC(this.msisdnC);
        clonedObject.setImeiA(this.imeiA);
        clonedObject.setImeiB(this.imeiB);
        clonedObject.setImsiA(this.imsiA);
        clonedObject.setImsiB(this.imsiB);
        return clonedObject;
    }
}
